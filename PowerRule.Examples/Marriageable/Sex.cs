﻿namespace PowerRule.Examples.Marriageable;

public enum Sex
{
    Male,
    Female
}