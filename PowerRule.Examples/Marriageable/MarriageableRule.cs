﻿using PowerRule.Examples.Marriageable.Internal;

namespace PowerRule.Examples.Marriageable;

public static class MarriageableRule
{
    public static readonly Rule<Person> Rule =
        ~new MarriedRule() & (
            (new SexRule(Sex.Male) & AgeRule.GreaterOrEquals(18)) |
            (new SexRule(Sex.Female) & AgeRule.GreaterOrEquals(16) & DaysDivorcedRule.GreaterOrEquals(100))
        );
}