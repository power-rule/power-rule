﻿namespace PowerRule.Examples.Marriageable;

public sealed class MarriageableRuleTest
{
    private readonly Rule<Person> _target;

    public MarriageableRuleTest()
    {
        _target = MarriageableRule.Rule;
    }

    [Fact]
    public void TestMaleUnder18()
    {
        PAssert.IsTrue(() => !_target.Verify(new Person
        {
            Sex = Sex.Male,
            Age = 17,
            IsMarried = false
        }));
    }

    [Fact]
    public void TestMaleOver18()
    {
        PAssert.IsTrue(() => _target.Verify(new Person
        {
            Sex = Sex.Male,
            Age = 18,
            IsMarried = false
        }));
    }

    [Fact]
    public void TestMaleAlreadyMarried()
    {
        PAssert.IsTrue(() => !_target.Verify(new Person
        {
            Sex = Sex.Male,
            Age = 18,
            IsMarried = true
        }));
    }

    [Fact]
    public void TestFemaleUnder16()
    {
        PAssert.IsTrue(() => !_target.Verify(new Person
        {
            Sex = Sex.Female,
            Age = 15,
            IsMarried = false
        }));
    }

    [Fact]
    public void TestFemaleOver16()
    {
        PAssert.IsTrue(() => _target.Verify(new Person
        {
            Sex = Sex.Female,
            Age = 16,
            IsMarried = false
        }));
    }

    [Fact]
    public void TestFemaleAlreadyMarried()
    {
        PAssert.IsTrue(() => !_target.Verify(new Person
        {
            Sex = Sex.Female,
            Age = 16,
            IsMarried = true
        }));
    }

    [Fact]
    public void TestFemaleNotPast100DaysSinceDivorced()
    {
        PAssert.IsTrue(() => !_target.Verify(new Person
        {
            Sex = Sex.Female,
            Age = 16,
            IsMarried = false,
            DaysDivorced = 99
        }));
    }
    
    [Fact]
    public void TestFemalePast100DaysSinceDivorced()
    {
        PAssert.IsTrue(() => _target.Verify(new Person
        {
            Sex = Sex.Female,
            Age = 16,
            IsMarried = false,
            DaysDivorced = 100
        }));
    }
}