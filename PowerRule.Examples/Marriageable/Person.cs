﻿namespace PowerRule.Examples.Marriageable;

public sealed record Person
{
    public required Sex Sex { get; init; }
    public required int Age { get; init; }
    public required bool IsMarried { get; init; }
    public int? DaysDivorced { get; init; }
}