﻿namespace PowerRule.Examples.Marriageable.Internal;

internal sealed class AgeRule : Rule<Person>
{
    private readonly Func<Person, bool> _predicate;

    private AgeRule(Func<Person, bool> predicate)
    {
        _predicate = predicate;
    }

    public override VerifyResult Verify(Person obj)
    {
        if (_predicate(obj)) return Valid();
        return Invalid();
    }

    public static Rule<Person> GreaterOrEquals(int value)
    {
        return new AgeRule(person => person.Age >= value);
    }
}