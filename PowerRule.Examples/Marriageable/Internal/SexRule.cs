﻿namespace PowerRule.Examples.Marriageable.Internal;

internal sealed class SexRule : Rule<Person>
{
    private readonly Sex _sex;

    public SexRule(Sex sex)
    {
        _sex = sex;
    }

    public override VerifyResult Verify(Person obj)
    {
        if (_sex == obj.Sex) return Valid();
        return Invalid();
    }
}