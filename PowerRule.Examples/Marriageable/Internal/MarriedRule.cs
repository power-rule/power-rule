﻿namespace PowerRule.Examples.Marriageable.Internal;

internal sealed class MarriedRule : Rule<Person>
{
    public override VerifyResult Verify(Person obj)
    {
        if (obj.IsMarried) return Valid();
        return Invalid();
    }
}