﻿namespace PowerRule.Examples.Marriageable.Internal;

internal sealed class DaysDivorcedRule : Rule<Person>
{
    private readonly Func<Person, bool> _predicate;

    private DaysDivorcedRule(Func<Person, bool> predicate)
    {
        _predicate = predicate;
    }

    public static Rule<Person> GreaterOrEquals(int value)
    {
        return new DaysDivorcedRule(person => (person.DaysDivorced ?? int.MaxValue) >= value);
    }

    public override VerifyResult Verify(Person obj)
    {
        if (_predicate(obj)) return Valid();
        return Invalid();
    }
}