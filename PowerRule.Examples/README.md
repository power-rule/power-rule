﻿# PowerRule Examples
| Rule                                                   | Test                                                           | Description                           |
|--------------------------------------------------------|----------------------------------------------------------------|---------------------------------------|
| [MarriageableRule](./Marriageable/MarriageableRule.cs) | [MarriageableRuleTest](./Marriageable/MarriageableRuleTest.cs) | Marriageability Requirements in Japan |