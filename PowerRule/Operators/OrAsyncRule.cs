﻿using System.Threading.Tasks;

namespace PowerRule.Operators;

public sealed class OrAsyncRule<T> : AsyncRule<T>
{
    private readonly AsyncRule<T> _left;
    private readonly AsyncRule<T> _right;

    public OrAsyncRule(AsyncRule<T> left, AsyncRule<T> right)
    {
        _left = left;
        _right = right;
    }

    public override async Task<VerifyResult> VerifyAsync(T obj)
    {
        var leftResult = await _left.VerifyAsync(obj);
        if (leftResult) return Valid();

        return await _right.VerifyAsync(obj);
    }
}