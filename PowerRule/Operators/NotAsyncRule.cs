﻿using System.Threading.Tasks;

namespace PowerRule.Operators;

public sealed class NotAsyncRule<T> : AsyncRule<T>
{
    private readonly AsyncRule<T> _rule;

    public NotAsyncRule(AsyncRule<T> rule)
    {
        _rule = rule;
    }

    public override async Task<VerifyResult> VerifyAsync(T obj)
    {
        if (await _rule.VerifyAsync(obj)) return Invalid();
        return Valid();
    }
}