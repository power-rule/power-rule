﻿namespace PowerRule.Operators;

public sealed class NotRule<T> : Rule<T>
{
    private readonly Rule<T> _rule;

    public NotRule(Rule<T> rule)
    {
        _rule = rule;
    }

    public override VerifyResult Verify(T obj)
    {
        if (_rule.Verify(obj)) return Invalid();
        return Valid();
    }
}