namespace PowerRule.Operators;

public sealed class OrRule<T> : Rule<T>
{
    private readonly Rule<T> _left;
    private readonly Rule<T> _right;

    public OrRule(Rule<T> left, Rule<T> right)
    {
        _left = left;
        _right = right;
    }

    public override VerifyResult Verify(T obj)
    {
        if (_left.Verify(obj) || _right.Verify(obj)) return Valid();
        return Invalid();
    }
}