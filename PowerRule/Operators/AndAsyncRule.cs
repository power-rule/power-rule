﻿using System.Threading.Tasks;

namespace PowerRule.Operators;

public sealed class AndAsyncRule<T> : AsyncRule<T>
{
    private readonly AsyncRule<T> _left;
    private readonly AsyncRule<T> _right;

    public AndAsyncRule(AsyncRule<T> left, AsyncRule<T> right)
    {
        _left = left;
        _right = right;
    }

    public override async Task<VerifyResult> VerifyAsync(T obj)
    {
        var leftResult = await _left.VerifyAsync(obj);
        if (!leftResult) return Invalid();

        return await _right.VerifyAsync(obj);
    }
}