using PowerRule.Operators;

namespace PowerRule;

public abstract class Rule<T>
{
    // ReSharper disable once UnusedParameter.Global
    public abstract VerifyResult Verify(T obj);

    public static Rule<T> operator &(Rule<T> left, Rule<T> right)
    {
        return new AndRule<T>(left, right);
    }

    public static Rule<T> operator |(Rule<T> left, Rule<T> right)
    {
        return new OrRule<T>(left, right);
    }

    public static Rule<T> operator ~(Rule<T> rule)
    {
        return new NotRule<T>(rule);
    }

    protected VerifyResult Valid()
    {
        return VerifyResult.Valid();
    }

    protected VerifyResult Invalid()
    {
        return VerifyResult.Invalid();
    }
}