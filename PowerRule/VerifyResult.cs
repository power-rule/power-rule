﻿using System;

namespace PowerRule;

public sealed class VerifyResult : IEquatable<VerifyResult>
{
    private VerifyResult(bool isValid)
    {
        IsValid = isValid;
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public bool IsValid { get; }
    
    public static bool operator true(VerifyResult value)
    {
        return value.IsValid;
    }

    public static bool operator false(VerifyResult value)
    {
        return !value.IsValid;
    }

    public static implicit operator bool(VerifyResult value)
    {
        return value.IsValid;
    }

    public static VerifyResult Valid()
    {
        return new VerifyResult(true);
    }

    public static VerifyResult Invalid()
    {
        return new VerifyResult(false);
    }
    
    public bool Equals(VerifyResult? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return IsValid == other.IsValid;
    }

    public override int GetHashCode()
    {
        return IsValid.GetHashCode();
    }

    public override string ToString()
    {
        if (IsValid) return "Valid";
        return "Invalid";
    }
}