﻿using System.Threading.Tasks;
using PowerRule.Operators;

namespace PowerRule;

public abstract class AsyncRule<T>
{
    // ReSharper disable once UnusedParameter.Global
    public abstract Task<VerifyResult> VerifyAsync(T obj);

    public static AsyncRule<T> operator &(AsyncRule<T> left, AsyncRule<T> right)
    {
        return new AndAsyncRule<T>(left, right);
    }
    
    public static AsyncRule<T> operator |(AsyncRule<T> left, AsyncRule<T> right)
    {
        return new OrAsyncRule<T>(left, right);
    }

    public static AsyncRule<T> operator ~(AsyncRule<T> rule)
    {
        return new NotAsyncRule<T>(rule);
    }

    public static implicit operator AsyncRule<T>(Rule<T> rule)
    {
        return new AsyncRuleAdapter<T>(rule);
    }

    protected VerifyResult Valid()
    {
        return VerifyResult.Valid();
    }

    protected VerifyResult Invalid()
    {
        return VerifyResult.Invalid();
    }
}
