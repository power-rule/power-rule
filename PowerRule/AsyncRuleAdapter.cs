﻿using System.Threading.Tasks;

namespace PowerRule;

internal sealed class AsyncRuleAdapter<T> : AsyncRule<T>
{
    private readonly Rule<T> _rule;

    public AsyncRuleAdapter(Rule<T> rule)
    {
        _rule = rule;
    }

    public override Task<VerifyResult> VerifyAsync(T obj)
    {
        return Task.FromResult(_rule.Verify(obj));
    }
}