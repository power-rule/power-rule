﻿# PowerRule
PowerRule is a validation library that is stable, easy to customize, and the rules it defines are easy to understand.

There is no need to stare at the documentation, as is often the case with validation libraries.


## How to use
Define custom rules class. Simply inherit [Rule&lt;T>](./PowerRule/Rule.cs).

```csharp
class SexRule : Rule<Person>
{
    private readonly Sex _sex;

    public SexRule(Sex sex)
    {
        _sex = sex;
    }

    public override VerifyResult Verify(Person obj)
    {
        if (_sex == obj.Sex) return Valid();
        return Invalid();
    }
}
```

The rules are defined by combining those rules. You can use negation (~), and (&), or (|), and parentheses.

```csharp
var marriageableRule =
    ~new MarriedRule() & (
        (new SexRule(Sex.Male) & AgeRule.GreaterOrEquals(18)) |
        (new SexRule(Sex.Female) & AgeRule.GreaterOrEquals(16) & DaysDivorcedRule.GreaterOrEquals(100))
    );
```

Use it.

```csharp
var person = new Person
{
    Sex = Sex.Male,
    Age = 18,
    IsMarried = false
};

if (marriageableRule.Verify(person))
{
    Console.WriteLine("OK");
}
```

See [examples](./PowerRule.Examples).

### Asynchronous version
AsyncRule class.

```csharp
class SexAsyncRule : AsyncRule<string>
{
    public override async Task<VerifyResult> Verify(string userId)
    {
        var person = await UserManager.FindAsync(userId);
        if (person.Sex == _sex) return Valid();
        return Invalid();
    }
}
```

Verify.

```csharp
if (await marriageableRule.VerifyAsync("user01"))
{
    Console.WriteLine("OK");
}
```

### Mix synchronous and asynchronous
The Synchronous Rule and the Asynchronous Rule are combined to form the Asyncrhnous Rule.

```csharp
// This is AsyncRule.
var rule = new SyncCustomRule() & new AsyncCustomRule();

if (await rule.VerifyAsync("hello")) {
    Console.WriteLine("OK");
}
```

## Install PowerRule to your .NET project
```
$ dotnet add package PowerRule
```

Install `PowerRule.Async` if you need the asynchronous version.

```
$ dotnet add package PowerRule.Async
```


## LICENSE
[MIT](./LICENSE)
