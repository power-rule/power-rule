using Builds;
using Nuke.Common;

class Build : NukeBuild, ITest, IInspection, IPush
{
    public static int Main() => Execute<Build>(x => ((ICompile) x).Compile);
}