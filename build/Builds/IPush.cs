﻿using System;
using System.IO;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using Serilog;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface IPush : IPackage
{
    [Parameter($"NuGet API Key.")]
    string NugetApiKey => TryGetValue(() => NugetApiKey);

    [Parameter("Push NuGet source. Default is `nuget.org`.")]
    string NugetSource => TryGetValue(() => NugetSource) ?? "nuget.org";

    // ReSharper disable once UnusedMember.Global
    Target Push => _ => _
        .DependsOn(Pack)
        .Executes(() =>
        {
            DotNetNuGetPush(s => s
                .SetTargetPath(DistDirectory / "*.nupkg")
                .SetApiKey(NugetApiKey)
                .SetSource(NugetSource)
            );
        });

    // ReSharper disable once UnusedMember.Global
    Target NugetOrgDotEnv => _ => _
        .Unlisted()
        .TriggeredBy(Push)
        .OnlyWhenStatic(() => NugetSource == "nuget.org")
        .Executes(() =>
        {
            var packageVersion = GitVersion.NuGetVersionV2;
            var packageFullName = $"PowerRule.{packageVersion}";
            
            var dotenv = $"""
                          NUPKG_FILE="{packageFullName}.nupkg"
                          SNUPKG_FILE="{packageFullName}.snupkg"
                          NUGET_ORG_NUPKG_DOWNLOAD_URL="https://www.nuget.org/api/v2/package/PowerRule/{packageVersion}"
                          NUGET_ORG_SNUPKG_DOWNLOAD_URL="https://www.nuget.org/api/v2/symbolpackage/PowerRule/{packageVersion}"
                          """;
            (DistDirectory / "nuget-org.env").WriteAllText(dotenv);
        });
}