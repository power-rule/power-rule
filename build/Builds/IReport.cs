﻿using Nuke.Common;
using Nuke.Common.IO;

namespace Builds;

public interface IReport : INukeBuild
{
    AbsolutePath ReportDirectory => RootDirectory / "reports";
}