﻿using Nuke.Common;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface ITest : ICompile, IReport
{
    // ReSharper disable once UnusedMember.Global
    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var reports = ReportDirectory / "junit" / "{assembly}-test-result.xml";

            DotNetTest(s => s
                .SetProjectFile(Solution)
                .EnableNoRestore()
                .EnableNoBuild()
                .SetConfiguration(this.Configuration)
                .SetTestAdapterPath(".")
                .AddLoggers($"junit;LogFilePath={reports};MethodFormat=Class;FailureBodyFormat=Verbose"));
        });
}