﻿using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;

namespace Builds;

public interface ISolution : INukeBuild
{
    [Solution] Solution Solution => TryGetValue(() => Solution);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    Configuration Configuration =>
        TryGetValue(() => Configuration) ?? (IsLocalBuild ? Configuration.Debug : Configuration.Release);
    
    AbsolutePath CacheDirectory => RootDirectory / ".cache";
}