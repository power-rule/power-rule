﻿using System.Text.Json;
using Nuke.Common;
using Nuke.Common.Tools.GitVersion;
using Serilog;

namespace Builds;

public interface IGit : INukeBuild
{
    [GitVersion] GitVersion GitVersion => TryGetValue(() => GitVersion);

    // ReSharper disable once UnusedMember.Global
    Target Version => _ => _
        .Executes(() =>
        {
            Log.Information("{@Version}", JsonSerializer.Serialize(GitVersion, new JsonSerializerOptions
            {
                WriteIndented = true
            }));
        });
}