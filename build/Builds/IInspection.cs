﻿using Ignis.ReSharper.Reporter.InspectCode.Convert.CodeQuality;
using Ignis.ReSharper.Reporter.InspectCode.Convert.Summary;
using Ignis.ReSharper.Reporter.InspectCode.Validations.SeverityLevel;
using Ignis.ReSharper.Reporter.Nuke;
using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;
using static Ignis.ReSharper.Reporter.Nuke.ReSharperReporterTasks;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

namespace Builds;

public interface IInspection : ICompile, IReport
{
    // ReSharper disable once UnusedMember.Global
    Target Inspect => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var reports = new
            {
                ReSharper = ReportDirectory / "resharper" / "inspect-code.xml",
                CodeQuality = ReportDirectory / "gitlab" / "gl-code-quality-report.json"
            };

            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .AddProperty("Configuration", Configuration)
                .SetOutput(reports.ReSharper)
                .SetCachesHome(CacheDirectory / "resharper" / "inspect-code")
                .SetProcessArgumentConfigurator(a => a
                    .Add("--no-build")));

            ReSharperReport(s => s
                .SetInput(reports.ReSharper)
                .SetSeverity(EnsureSeverityLevel.All)
                .AddExport<CodeQualityConverter>(reports.CodeQuality)
                .AddExport<SummaryConverter>());
        });
}