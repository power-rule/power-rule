﻿using System.Collections.Immutable;
using System.Linq;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface IPackage : ICompile
{
    AbsolutePath DistDirectory => RootDirectory / "dist";
    
    // ReSharper disable once UnusedMember.Global
    Target Pack => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var projects = new[]
                {
                    "PowerRule"
                }
                .Select(project => Solution.GetProject(project))
                .ToImmutableArray();

            DotNetPack(s => s
                .SetConfiguration(Configuration)
                .SetOutputDirectory(DistDirectory)
                .SetProperty("EmbedUntrackedSources", true)
                .SetProperty("IncludeSymbols", true)
                .SetProperty("SymbolPackageFormat", "snupkg")
                .SetProperty("PackageProjectUrl", "https://gitlab.com/power-rule/power-rule")
                .SetProperty("RepositoryURL", "https://gitlab.com/power-rule/power-rule.git")
                .SetProperty("RepositoryType", "git")
                .SetProperty("PackageLicenseExpression", "MIT")
                .SetProperty("PackageTags", "validation")
                .SetProperty("Description", "PowerRule is a validation library that easy to use.")
                .EnableNoBuild()
                .SetVersion(GitVersion.NuGetVersionV2)
                .CombineWith(projects, (s, project) => s.SetProject(project))
            );
        });
}