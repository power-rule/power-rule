﻿namespace PowerRule;

public static class TestData
{
    public static IEnumerable<object[]> And()
    {
        // { result1, result2, expected }
        yield return new object[] { true, true, true };
        yield return new object[] { true, false, false };
        yield return new object[] { false, true, false };
        yield return new object[] { false, false, false };
    }

    public static IEnumerable<object[]> Or()
    {
        // { result1, result2, expected }
        yield return new object[] { true, true, true };
        yield return new object[] { true, false, true };
        yield return new object[] { false, true, true };
        yield return new object[] { false, false, false };
    }

    public static IEnumerable<object[]> Not()
    {
        // { result, expected }
        yield return new object[] { true, false };
        yield return new object[] { false, true };
    }
}