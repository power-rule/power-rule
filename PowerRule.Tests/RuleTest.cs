namespace PowerRule;

public sealed class RuleTest
{
    [Theory]
    [MemberData(nameof(TestData.And), MemberType = typeof(TestData))]
    public void TestAnd(bool result1, bool result2, bool expected)
    {
        var left = MockRule.Rule("hello", result1);
        var right = MockRule.Rule("hello", result2);

        var target = left & right;

        PAssert.IsTrue(() => target.Verify("hello") == expected);
    }

    [Fact]
    public void TestAndShortCircuit()
    {
        var left = MockRule.Rule("hello", false);
        var right = MockRule.Rule("hello", true);

        var target = left & right;
        target.Verify("hello");

        right.DidNotReceiveWithAnyArgs().Verify(Arg.Any<string>());
    }

    [Theory]
    [MemberData(nameof(TestData.Or), MemberType = typeof(TestData))]
    public void TestOr(bool result1, bool result2, bool expected)
    {
        var left = MockRule.Rule("hello", result1);
        var right = MockRule.Rule("hello", result2);

        var target = left | right;

        PAssert.IsTrue(() => target.Verify("hello") == expected);
    }

    [Fact]
    public void TestOrShortCircuit()
    {
        var left = MockRule.Rule("hello", true);
        var right = MockRule.Rule("hello", false);

        var target = left | right;
        target.Verify("hello");

        right.DidNotReceiveWithAnyArgs().Verify(Arg.Any<string>());
    }

    [Theory]
    [MemberData(nameof(TestData.Not), MemberType = typeof(TestData))]
    public void TestNot(bool result, bool expected)
    {
        var target = ~MockRule.Rule("hello", result);

        PAssert.IsTrue(() => target.Verify("hello") == expected);
    }
}