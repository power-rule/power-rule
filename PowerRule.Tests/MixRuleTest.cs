﻿namespace PowerRule;

public sealed class MixRuleTest
{
    [Fact]
    public async Task TestAsyncAndSync()
    {
        var target = MockRule.AsyncRule("hello1", true) &
                     MockRule.Rule("hello1", true);

        var actual = await target.VerifyAsync("hello1");
        
        PAssert.IsTrue(() => actual);
    }

    [Fact]
    public async Task TestSyncAndAsync()
    {
        var target = MockRule.Rule("hello1", true) &
                     MockRule.AsyncRule("hello1", true);

        var actual = await target.VerifyAsync("hello1");
        
        PAssert.IsTrue(() => actual);
    }
}