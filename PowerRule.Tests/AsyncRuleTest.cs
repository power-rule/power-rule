﻿namespace PowerRule;

public sealed class AsyncRuleTest
{
    [Theory]
    [MemberData(nameof(TestData.And), MemberType = typeof(TestData))]
    public async Task TestAnd(bool result1, bool result2, bool expected)
    {
        var left = MockRule.AsyncRule("hello", result1);
        var right = MockRule.AsyncRule("hello", result2);
        
        var target = left & right;

        var actual = await target.VerifyAsync("hello");

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestAndShortCircuit()
    {
        var left = MockRule.AsyncRule("hello", false);
        var right = MockRule.AsyncRule("hello", true);

        var target = left & right;
        target.VerifyAsync("hello");

        right.DidNotReceiveWithAnyArgs().VerifyAsync(Arg.Any<string>());
    }

    [Theory]
    [MemberData(nameof(TestData.Or), MemberType = typeof(TestData))]
    public async Task TestOr(bool result1, bool result2, bool expected)
    {
        var left = MockRule.AsyncRule("hello", result1);
        var right = MockRule.AsyncRule("hello", result2);
        
        var target = left | right;

        var actual = await target.VerifyAsync("hello");

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestOrShortCircuit()
    {
        var left = MockRule.AsyncRule("hello", true);
        var right = MockRule.AsyncRule("hello", false);

        var target = left | right;
        target.VerifyAsync("hello");

        right.DidNotReceiveWithAnyArgs().VerifyAsync(Arg.Any<string>());
    }

    [Theory]
    [MemberData(nameof(TestData.Not), MemberType = typeof(TestData))]
    public async Task TestNot(bool result, bool expected)
    {
        var target = ~MockRule.AsyncRule("hello", result);

        var actual = await target.VerifyAsync("hello");
        
        PAssert.IsTrue(() => actual == expected);
    }
}