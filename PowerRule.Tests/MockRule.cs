﻿namespace PowerRule;

public static class MockRule
{
    public static Rule<T> Rule<T>(T obj, bool result)
    {
        var mock = Substitute.For<Rule<T>>();
        mock.Verify(obj).Returns(result ? VerifyResult.Valid() : VerifyResult.Invalid());
        return mock;
    }

    public static AsyncRule<T> AsyncRule<T>(T obj, bool result)
    {
        var mock = Substitute.For<AsyncRule<T>>();
        mock.VerifyAsync(obj).Returns(result ? VerifyResult.Valid() : VerifyResult.Invalid());
        return mock;
    }
}